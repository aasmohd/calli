package com.example.hp.calli;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Contacts;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;

/**
 * Created by osr on 2/2/18.
 */

public class util {
    Context context;

    public util(Context context) {

        this.context = context;
    }

    public String getContactName(final String phoneNumber) {
        Uri uri;
        String[] projection;
        Uri mBaseUri = Contacts.Phones.CONTENT_FILTER_URL;
        projection = new String[]{Contacts.People.NAME};
        try {
            Class<?> c = Class.forName("android.provider.ContactsContract$PhoneLookup");
            mBaseUri = (Uri) c.getField("CONTENT_FILTER_URI").get(mBaseUri);
            projection = new String[]{"display_name"};
        } catch (Exception e) {
        }

        uri = Uri.withAppendedPath(mBaseUri, Uri.encode(phoneNumber));
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

        String contactName = "";

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(0);
        }
        else contactName=phoneNumber;

        cursor.close();
        cursor = null;
        return contactName;
    }


    public void displayNotificationSingleLine(String number) {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);// Setting builder
        NotificationManager mNotificationManager;
        final int notificationID_SingleLine = 111;
        int numMessages_SingleLine = 0;
        Uri alarmSound;
        final long[] pattern = {100, 300, 300, 300};

        alarmSound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder.setContentTitle(getContactName(number));// title
        mBuilder.setContentText("Call Recorded");// Message
        mBuilder.setTicker("New Message Alert!");
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setNumber(++numMessages_SingleLine);
        mBuilder.setSound(alarmSound);
        mBuilder.setVibrate(pattern);
        mBuilder.setAutoCancel(true);//
        Intent resultIntent = new Intent(context,
                MainActivity.class);

        resultIntent.putExtra("notificationId", notificationID_SingleLine);
        resultIntent.putExtra("message", "http://androhub.com");
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        mNotificationManager
                .notify(notificationID_SingleLine, mBuilder.build());

    }


}
